# README #
This README is a test to see how we can work with file under

### What is this repository for? ###

* To better learn how to use version control and bitbucket
* Versioning of source code
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure you have a bitbucket account and are logged in
* Clone this project, make sure you set the local path to be in your Eclips workspace folder
* Once the files are downloaded, run Eclips and goto File -> Import. Locate the new project folder in your workspace folder and import that.

### Other tasks to learn ###

* Branches and Mergers
* Forking
* Teams and Projects
* [Issue Tracking] (https://bitbucket.org/Dr-WaSaBi/mecanum-drive/issues)
* Improved documentation / Wiki
* Team communications
* 


### Contribution guidelines ###

* Do we have team standards for code?  if so where are they?
* 
### Who do I talk to? ###

* Talk to Sam <Edit this file and add your email>
* Talk with Mr. Johnson <Edit this file and add your email>
* Talk to Mr. Riker <russellriker@gmail.com>

* Other community or team contact